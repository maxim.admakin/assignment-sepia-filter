#include "../include/image.h"

struct image image_create(uint64_t height, uint64_t width) {
    struct pixel* data = malloc(sizeof(struct pixel) * height * width);
    return (struct image){.height = height, .width = width, .data = data};
}

static inline uint64_t image_from_coords(struct image* image, uint64_t x, uint64_t y) {
    return image->width * y + x;
}

struct pixel image_get_pixel(struct image* image, uint64_t x, uint64_t y) {
    return image->data[image_from_coords(image, x, y)];
}

void image_set_pixel(struct image* image, struct pixel pixel, uint64_t x, uint64_t y) {
    image->data[image_from_coords(image, x, y)] = pixel;
}

void image_destroy(struct image* image) { free(image->data); }
