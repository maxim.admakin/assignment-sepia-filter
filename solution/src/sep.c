#include "../include/sep.h"

#include <inttypes.h>

static unsigned char sat(uint64_t x) {
    if (x < 256) return x;
    return 255;
}

static struct pixel sepia_one_native(struct pixel const pixel) {
    static const float c[9] = {.393f, .769f, .189f, .349f, .686f, .168f, .272f, .543f, .131f};
    return (struct pixel){
        .red = sat(pixel.red * c[0] + pixel.green * c[1] + pixel.blue * c[2]),
        .green = sat(pixel.red * c[3] + pixel.green * c[4] + pixel.blue * c[5]),
        .blue = sat(pixel.red * c[6] + pixel.green * c[7] + pixel.blue * c[8]),
    };
}

struct image sepia_native(struct image source) {
    struct image result = image_create(source.height, source.width);

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            image_set_pixel(&result, sepia_one_native(image_get_pixel(&source, x, y)), x, y);
        }
    }

    return result;
}

extern void sepia_a_sse(struct pixel *source, struct pixel *target, uint64_t count);

struct image sepia_sse(struct image source) {
    struct image result = image_create(source.height, source.width);

    uint64_t pixel_count = source.width * source.height;
    uint64_t chunks = pixel_count / 4;
    if (chunks > 0) sepia_a_sse(source.data, result.data, chunks);

    for (uint64_t i = chunks * 4; i < pixel_count; i++)
        result.data[i] = sepia_one_native(source.data[i]);

    return result;
}