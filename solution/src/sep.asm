global sepia_a_sse

section .text

sepia_a_sse:
    test rdx, rdx
    jnz .begin
    ret

    .begin:
        push r12
        push r13
        mov rcx, 0xFF000000

    .loop:
        mov eax, [rdi+0]
        mov r8, rax
        mov r9, rax
        mov r10, rax
        shr r9, 8
        shr r10, 16
        shr rax, 16
        and r8, 0x000000FF
        and r9, 0x000000FF
        and r10, 0x000000FF
        and rax, 0x0000FF00
        or r8, rax

        mov eax, [rdi+4]
        mov r11, rax
        mov r12, rax
        mov r13, rax
        shl r11, 8
        shr rax, 8
        and r11, 0x0000FF00
        and r12, 0x0000FF00
        and r13, 0x00FF0000
        and rax, 0x00FF0000
        or r9, r11
        or r10, r12
        or r8, r13
        or r9, rax

        mov eax, [rdi+8]
        mov r11, rax
        mov r12, rax
        mov r13, rax
        shl r11, 16
        shl r12, 16
        shl r13, 8
        and r11, 0x00FF0000
        and r12, rcx 
        and r13, rcx 
        and rax, rcx 
        or r10, r11
        or r8, r12
        or r9, r13
        or r10, rax

        movq xmm0, r8
        movq xmm1, r9
        movq xmm2, r10
        pxor xmm6, xmm6

        punpcklbw xmm0, xmm6  
        punpcklbw xmm1, xmm6
        punpcklbw xmm2, xmm6

        punpcklwd xmm0, xmm6 
        punpcklwd xmm1, xmm6
        punpcklwd xmm2, xmm6

        cvtdq2ps xmm0, xmm0
        cvtdq2ps xmm1, xmm1
        cvtdq2ps xmm2, xmm2

        mov r11d, 1049314198  ; pre-calculated
        mov r12d, 1056964608  ; pre-calculated
        mov r13d, 1040590045  ; pre-calculated

        movq xmm3, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm3, xmm3 
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm3, xmm3 
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm3, xmm0 
        mulps xmm6, xmm1  
        mulps xmm7, xmm2  

        addps xmm3, xmm6  
        addps xmm3, xmm7  

        mov r11d, 1051025474  ; pre-calculated
        mov r12d, 1060085170  ; pre-calculated
        mov r13d, 1043073073  ; pre-calculated

        movq xmm4, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm4, xmm4
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm4, xmm4
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm4, xmm0
        mulps xmm6, xmm1
        mulps xmm7, xmm2

        addps xmm4, xmm6
        addps xmm4, xmm7 

        mov r11d, 1053374284  ; pre-calculated
        mov r12d, 1061226021  ; pre-calculated
        mov r13d, 1045287666  ; pre-calculated

        movq xmm5, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm5, xmm5
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm5, xmm5
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm5, xmm0
        mulps xmm6, xmm1
        mulps xmm7, xmm2

        addps xmm5, xmm6
        addps xmm5, xmm7  

        cvtps2dq xmm3, xmm3
        cvtps2dq xmm4, xmm4
        cvtps2dq xmm5, xmm5

        packssdw xmm3, xmm3
        packusdw xmm4, xmm4
        packusdw xmm5, xmm5

        packuswb xmm3, xmm3
        packuswb xmm4, xmm4
        packuswb xmm5, xmm5

        movq r8, xmm3   
        movq r9, xmm4   
        movq r10, xmm5 

        mov r11, r8
        mov r12, r9
        mov r13, r10
        mov rax, r8
        and r11, 0x000000FF
        and r12, 0x000000FF
        and r13, 0x000000FF
        and rax, 0x0000FF00
        shl r12, 8
        shl r13, 16
        shl rax, 16
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+0], eax

        mov r11, r9
        mov r12, r10
        mov r13, r8
        mov rax, r9
        and r11, 0x0000FF00
        and r12, 0x0000FF00
        and r13, 0x00FF0000
        and rax, 0x00FF0000
        shr r11, 8
        shl rax, 8
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+4], eax

        mov r11, r10
        mov r12, r8
        mov r13, r9
        mov rax, r10
        and r11, 0x00FF0000
        and r12, rcx ; 0xFF000000
        and r13, rcx ; 0xFF000000
        and rax, rcx ; 0xFF000000
        shr r11, 16
        shr r12, 16
        shr r13, 8
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+8], eax

        add rdi, 12
        add rsi, 12
        dec rdx
        jnz .loop

    pop r13
    pop r12
    ret
