#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include "include/bmp.h"
#include "include/image.h"
#include "include/sep.h"

void timed(char* prompt, struct image image, struct image (*f)(struct image)) {
    static const int rep = 1000;
    struct rusage r;

    getrusage(RUSAGE_SELF, &r);
    struct timeval temp = r.ru_utime;

    struct image new_image;
    for (int i = 0; i < rep; i++) new_image = f(image);
    image_destroy(&new_image);

    getrusage(RUSAGE_SELF, &r);
    printf("Time elapsed for %d repeats of %s conversion: %ld (ms)\n", rep, prompt,
           ((r.ru_utime.tv_sec - temp.tv_sec) * 1000000L) + r.ru_utime.tv_usec - temp.tv_usec);
}

int main(void) {
    char* input_file_name = "files/input.bmp";

    FILE* fin = fopen(input_file_name, "rb");

    struct image image = {0};
    from_bmp(fin, &image);

    timed("c", image, sepia_native);
    timed("asm", image, sepia_sse);
    
    image_destroy(&image);
    fclose(fin);

    return  0;
}