#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "include/bmp.h"
#include "include/image.h"
#include "include/sep.h"

int main(int argc, char** argv) {
    if (argc < 3) 
    {
        return 1;
    }
    char* input_file_name = argv[1];
    char* output_file_name_native = argv[2];
    char* output_file_name_sse = argv[3];

    FILE* fin = fopen(input_file_name, "rb");

    FILE* fout_native = fopen(output_file_name_native, "wb");

    FILE* fout_sse = fopen(output_file_name_sse, "wb");

    struct image image = {0};
    from_bmp(fin, &image);

    struct image new_image_native = sepia_native(image);
    to_bmp(fout_native, &new_image_native);
    image_destroy(&new_image_native);
    

    struct image new_image_sse = sepia_sse(image);
    to_bmp(fout_sse, &new_image_sse);

    image_destroy(&image);
    fclose(fin);
    fclose(fout_native);
    fclose(fout_sse);

    return 0;
}
