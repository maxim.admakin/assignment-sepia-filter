#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

struct image sepia_native(struct image source);

struct image sepia_sse(struct image img);

#endif
